#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numbers

import cairo
import numpy as np
import numpy.ma as ma

BORDER = 4
Y_PADDING = 0.7
X_PADDING = 1.5
X_OFFSET = 0.5
Y_OFFSET = 0.9
DEFAULT_FONT = 'Charter'
VARIABLE_FONT = 'Charter'
DEFAULT_FONT_SIZE = 10
SMALL_FONT_SIZE = 7
EXTRA_SMALL_FONT_SIZE = 5

## Various colors from ColorBrewer
#COLORS = [
#    (228,  26,  28),
#    ( 55, 126, 184),
#    ( 77, 175,  74),
#    (152,  78, 163),
#    (255, 127,   0),
#    (166,  86,  40),
#    (247, 129, 191),
#    (153, 153, 153),
#]
## Convert 0..255 to 0.0..1.0
#COLORS = [[x / 255.0 for x in rgb] for rgb in COLORS]
COLORS = [
    (0.0, 0.0, 0.0),
    (0.5, 0.5, 0.5),
    (0.0, 0.0, 0.0),
    (0.5, 0.5, 0.5),
    (0.0, 0.0, 0.0),
    (0.5, 0.5, 0.5),
    (0.0, 0.0, 0.0),
    (0.5, 0.5, 0.5),
]

class Diagram:
    def __init__(
            self,
            name,
            layout,
            font_name=DEFAULT_FONT,
            font_size=DEFAULT_FONT_SIZE,
            font_slant=cairo.FontSlant.NORMAL,
            xs=None,
            ys=None,
            outputs=None,
            xlabel=False,
            ylabel=False,
            outlabel=False,
            color_offset=0,
            border=BORDER,
            x_padding=X_PADDING,
            y_padding=Y_PADDING,
            x_offset=X_OFFSET,
            y_offset=Y_OFFSET):
        self.name = name
        self.layout = layout
        self.font_name = font_name
        self.font_size = font_size
        self.font_slant = font_slant
        self.xs = xs
        self.ys = ys
        self.outputs = outputs
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.outlabel = outlabel
        self.color_offset = color_offset
        self.border = border

        # Set up a dummy context for initial measurements.
        dummy_surface = cairo.PDFSurface(None, 100, 100)
        self.ctx = cairo.Context(dummy_surface)
        self.ctx.select_font_face(self.font_name, self.font_slant)
        self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))

        # I *should* be using fheight and fdescent from `ctx.font_extents()`, but
        # they keep giving me ridiculously large values, so we'll just render a
        # capital "M" and assume it makes for a good enough reference point.
        xbearing, ybearing, width, height, xadvance, yadvance = self.ctx.text_extents('M')
        self.emwidth = width
        self.emheight = height

        # I use these a lot.
        (self.ls, self.rs, self.cs) = self.layout.shape

        self.x_padding = self.font_size * x_padding
        self.y_padding = self.font_size * y_padding
        self.x_offset = self.font_size * x_offset
        self.y_offset = self.font_size * y_offset
        
    def position(self):
        """Sets four properties:
         - row_pos, a (layer, row) indexed array of y positions for each row's
           baseline,
         - col_pos, a (layer, column) indexed array of x positions for each column's
           centerline,
         - right_pos, a float giving the x position of the rightmost element of the
           matrix, and
         - bottom_pos, a float giving the y position of the bottommost element of
           the matrix."""
        self.row_pos = ma.array(np.empty((self.ls, self.rs), dtype=np.float32), mask=True)
        self.col_pos = ma.array(np.empty((self.ls, self.cs), dtype=np.float32), mask=True)
    
        # Lay out all the rows
        cur_row_pos = -self.y_padding - self.y_offset + self.emheight
        for r in range(0, self.rs):
            header_row = True
            for l in range(0, self.ls):
                for c in range(0, self.cs):
                    if not self.layout.mask[l,r,c]:
                        cur_row_pos += self.y_offset
                        if header_row:
                            cur_row_pos += self.y_padding
                            header_row = False
                        self.row_pos[l,r] = cur_row_pos
                        break
        bottom_pos = cur_row_pos
                    
        # Start all of the columns the same way
        for l in range(0, self.ls):
            self.col_pos[l,:] = l * self.x_offset
    
        # Adjust the column locations so they're all next to each other
        for c in range(1, self.cs):
            self.adjust_column_position(c)
    
        # Shift all of the columns so that the leftmost one starts at 0.
        left, right = self.get_column_envelope(0)
        self.col_pos -= left
    
        # See what the rightmost position is.
        left, right_pos = self.get_column_envelope(self.cs-1)

        # Add space to the bottom for descenders
        bottom_descender = 0
        for c in range(0, self.cs):
            l = self.ls - 1
            r = self.rs - 1
            if not self.layout.mask[l,r,c]:
                text = str(self.layout[l,r,c])
                xbearing, ybearing, width, height, xadvance, yadvance = self.ctx.text_extents(text)
                descender = ybearing + height
                if descender > bottom_descender:
                    bottom_descender = descender
        bottom_pos += bottom_descender
        
        # Record the outer bounds of the main matrix.
        self.right_pos = right_pos
        self.bottom_pos = bottom_pos

        
    def adjust_column_position(self, col):
        offset = None
    
        for l in range(0, self.ls):
            for r in range(0, self.rs):
                if not self.layout.mask[l,r,col-1] and not self.layout.mask[l,r,col]:
                    lxbearing, lybearing, lwidth, lheight, lxadvance, lyadvance = \
                        self.ctx.text_extents(str(self.layout[l,r,col-1]))
                    rxbearing, rybearing, rwidth, rheight, rxadvance, ryadvance = \
                        self.ctx.text_extents(str(self.layout[l,r,col]))
                    col_left  = self.col_pos[l,col]   - lxbearing - lwidth / 2
                    ref_right = self.col_pos[l,col-1] - rxbearing + rwidth / 2
                    cell_offset = ref_right + self.x_padding - col_left
                    if offset is None or offset < cell_offset:
                        offset = cell_offset
    
        self.col_pos[:,col] += offset

        
    def get_column_envelope(self, col):
        """Returns leftmost and rightmost measurements of the column."""
    
        left  = self.col_pos[0,col]
        right = self.col_pos[0,col]
        
        for l in range(0, self.ls):
            for r in range(0, self.rs):
                if not self.layout.mask[l,r,col]:
                    text = str(self.layout[l,r,col])
                    xbearing, ybearing, width, height, xadvance, yadvance = \
                        self.ctx.text_extents(text)
                    center = self.col_pos[l,col]
                    this_left = center - width / 2
                    this_right = center + width / 2
                    if this_left < left:
                        left = this_left
                    if right < this_right:
                        right = this_right
                        
        return (left, right)

    
    def render(self):
        """Renders the matrix to a PDF file based on its name."""

        if self.xs is not None:
            x_space = self.emheight + self.y_padding * 2
            if len(self.xs) > self.cs - 1:
                x_padding = (len(self.xs) - self.cs + 1) * self.x_padding
                for x in self.xs[self.cs-2:]:
                    xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(x))
                    x_padding += fwidth + self.font_size
            else:
                x_padding = 0
        else:
            x_space = 0
            x_padding = 0

        if self.ys is not None:
            y_space = 0
            for y in self.ys:
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(y))
                if fwidth > y_space:
                    y_space = fwidth
            y_space += self.x_padding
            if len(self.ys) > self.rs - 1:
                y_padding = (len(self.ys) - self.rs + 1) * (self.y_padding * 2 + self.emheight)
            else:
                y_padding = 0
        else:
            y_space = 0
            y_padding = 0

        if self.outputs is not None:
            output_vert_space = self.emheight + self.y_padding * 2
            if self.outlabel:
                xbearing, ybearing, outwidth, fheight, xadvance, yadvance = self.ctx.text_extents('outputs')
            else:
                outwidth = 0
            xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(self.outputs[-1]))
            
            output_horiz_space = y_space + self.x_padding + \
                                 max(self.emwidth / 2 + self.x_offset * len(self.outputs) + fwidth / 2,
                                     outwidth)
        else:
            output_vert_space = 0
            output_horiz_space = 0

        # Replace self.ctx with the real one.
        surface = cairo.PDFSurface(
            '{}.pdf'.format(self.name),
            self.right_pos  + 2 * self.border + max(y_space, output_horiz_space) + x_padding,
            self.bottom_pos + 2 * self.border + max(x_space, output_vert_space)  + y_padding)
        self.ctx = cairo.Context(surface)
        self.ctx.select_font_face(self.font_name, self.font_slant)
        self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
        self.ctx.translate(self.border + x_padding, self.border + x_space)

        it = np.nditer(self.layout, flags=['multi_index'])
        while not it.finished:
            l, r, c = it.multi_index
            if not self.layout.mask[l,r,c]:
                self.render_cell(it[0], self.col_pos[l,c], self.row_pos[l,r], COLORS[l+self.color_offset])
            it.iternext()
    
        self.ctx.set_source_rgb(0, 0, 0)
        if self.xs is not None and len(self.xs) > 0:
            if self.xlabel:
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(self.xs[0]))
                self.ctx.select_font_face(DEFAULT_FONT)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
                self.ctx.move_to(self.col_pos[0,self.cs-2] + fwidth / 2 + self.emwidth, -self.y_padding * 2)
                self.ctx.show_text('←')
                self.ctx.select_font_face(VARIABLE_FONT, cairo.FontSlant.ITALIC)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
                self.ctx.show_text('x')
                self.ctx.select_font_face(self.font_name, self.font_slant)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
            for i, x in enumerate(self.xs):
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(x))
                if i < self.cs-1:
                    col = self.cs - i - 2
                    cur_pos = self.col_pos[self.first_unmasked_layer_for_column(col),col]
                else:
                    cur_pos -= self.font_size + self.x_padding
                self.ctx.move_to(cur_pos - xbearing - fwidth / 2, -self.y_padding * 2)
                self.ctx.show_text(str(x))

        if self.ys is not None:
            if self.ylabel:
                self.ctx.select_font_face(DEFAULT_FONT)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents('↓')
                self.ctx.move_to(self.right_pos + self.x_padding + self.emwidth / 2 - xbearing - fwidth / 2,
                                 self.row_pos[0,1] - self.emheight * 2)
                self.ctx.show_text('↓')
                self.ctx.rel_move_to(-xadvance + fwidth/2, -yadvance - fheight)
                self.ctx.select_font_face(VARIABLE_FONT, cairo.FontSlant.ITALIC)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents('y')
                self.ctx.rel_move_to(-fwidth / 2, 0)
                self.ctx.show_text('y')
                self.ctx.select_font_face(self.font_name, self.font_slant)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
            for i, y in enumerate(self.ys):
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(y))
                if i < self.rs-1:
                    row = i + 1
                    cur_pos = self.row_pos[self.first_unmasked_layer_for_row(row),row]
                else:
                    cur_pos += self.font_size + self.y_padding * 2
                self.ctx.move_to(self.right_pos + self.x_padding + self.emwidth / 2 - xbearing - fwidth / 2, cur_pos)
                self.ctx.show_text(str(y))

        if self.outputs is not None and len(self.outputs) > 0:
            if self.outlabel:
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(self.outputs[0]))
                self.ctx.move_to(self.right_pos + y_space, -self.y_padding * 2)
                self.ctx.select_font_face(VARIABLE_FONT, cairo.FontSlant.ITALIC)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents('outputs')
                self.ctx.show_text('outputs')
                self.ctx.rel_move_to(-xadvance + xbearing + fwidth / 2, -yadvance + ybearing + fheight + self.font_size)
                self.ctx.select_font_face(self.font_name, self.font_slant)
                self.ctx.set_font_matrix(cairo.Matrix(xx=self.font_size, yy=self.font_size))
            else:
                self.ctx.move_to(self.right_pos + self.x_padding + self.emwidth / 2, -self.y_padding * 2)
            for i, output in enumerate(self.outputs):
                xbearing, ybearing, fwidth, fheight, xadvance, yadvance = self.ctx.text_extents(str(output))
                self.ctx.set_source_rgb(*COLORS[i+self.color_offset])
                self.ctx.rel_move_to(-xbearing - fwidth / 2, 0)
                self.ctx.show_text(str(output))
                self.ctx.rel_move_to(-xadvance + xbearing + fwidth / 2 + self.x_offset, -yadvance + self.y_offset)

        surface.finish()

        
    def render_cell(self, value, x, y, color):
        if isinstance(value, numbers.Number) and value < 0:
            xbearing, ybearing, width, height, xadvance, yadvance = self.ctx.text_extents('-')
            negative_offset = width / 2
        else:
            negative_offset = 0
        text = str(value)
        xbearing, ybearing, width, height, xadvance, yadvance = self.ctx.text_extents(text)
        self.ctx.set_source_rgb(*color)
        self.ctx.move_to(x - xbearing - width / 2 - negative_offset, y)
        self.ctx.show_text(text)

        
    def first_unmasked_layer_for_column(self, col):
        for l in range(0, self.ls):
            for r in range(0, self.rs):
                if not self.layout.mask[l,r,col]:
                    return l
        assert False, 'Empty column.'

        
    def first_unmasked_layer_for_row(self, row):
        for l in range(0, self.ls):
            for c in range(0, self.cs):
                if not self.layout.mask[l,row,c]:
                    return l
        assert False, 'Empty row.'

        
DIAGRAMS = [
    Diagram('abcdefgh',
            ma.array(
                [[['b', 'd'],
                  ['a', 'c']],
                 [['f', 'h'],
                  ['e', 'g']]],
                mask=[False]),
            font_name=VARIABLE_FONT,
            font_slant=cairo.FontSlant.ITALIC),
    Diagram('op_add',
            ma.array(
                [[[1, 0],
                  [0, 1]],
                 [[0, 1],
                  [0, 0]]],
                mask=[False])),
    Diagram('op_subtract',
            ma.array(
                [[[1,  0],
                  [0, -1]],
                 [[0,  1],
                  [0,  0]]],
                mask=[False])),
    Diagram('op_multiply',
            ma.array(
                [[[0, 0],
                  [1, 0]],
                 [[0, 1],
                  [0, 0]]],
                mask=[False])),
    Diagram('op_divide',
            ma.array(
                [[[1, 0],
                  [0, 0]],
                 [[0, 0],
                  [0, 1]]],
                mask=[False])),
    Diagram('sqrt_6-coth_1-initial',
            ma.array(
                [[[1, 0],
                  [2, 0]],
                 [[0, 0],
                  [1, 1]]],
                mask=[False]),
            xlabel=True,
            xs=[1, 3, 5, '…'],
            ylabel=True,
            ys=[2, 2, 4, '⋮']),
    Diagram('sqrt_6-coth_1-step_2',
            ma.array(
                [[[1, 0],
                  [2, 0],
                  [5, 0]],
                 [[0, 0],
                  [1, 1],
                  [2, 2]]],
                mask=[False]),
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮']),
    Diagram('sqrt_6-coth_1-step_2a',
            ma.array(
                [[[2, 0]],
                 [[1, 1]]],
                mask=[False]),
            font_size=SMALL_FONT_SIZE,
            border=0,
            x_padding=0.9),
    Diagram('sqrt_6-coth_1-step_2b',
            ma.array(
                [[[1, 0]],
                 [[0, 0]]],
                mask=[False]),
            font_size=SMALL_FONT_SIZE,
            border=0,
            x_padding=0.9),
    Diagram('sqrt_6-coth_1-step_2c',
            ma.array(
                [[[5, 0]],
                 [[2, 2]]],
                mask=[False]),
            font_size=SMALL_FONT_SIZE,
            border=0,
            x_padding=0.9),
    Diagram('sqrt_6-coth_1-step_3',
            ma.masked_values(
                [[[9999, 1, 0],
                  [   2, 2, 0],
                  [   5, 5, 0]],
                 [[9999, 0, 0],
                  [   2, 1, 1],
                  [   4, 2, 2]]],
                9999),
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮']),
    Diagram('sqrt_6-coth_1-step_4',
            ma.masked_values(
                [[[9999, 9999, 1, 0],
                  [   8,    2, 2, 0],
                  [  20,    5, 5, 0]],
                 [[9999, 9999, 0, 0],
                  [   7,    2, 1, 1],
                  [  14,    4, 2, 2]]],
                9999),
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮']),
    Diagram('sqrt_6-coth_1-step_5',
            ma.masked_values(
                [[[9999, 9999,    1,    0],
                  [   8,    2,    2,    0],
                  [  20,    5,    5,    0]],
                 [[9999, 9999,    0,    0],
                  [   7,    2,    1,    1],
                  [  14,    4,    2,    2]],
                 [[9999, 9999, 9999, 9999],
                  [   1,    0, 9999, 9999],
                  [   6,    1, 9999, 9999]]],
                9999),
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮'],
            outlabel=True,
            outputs=[1]),
    Diagram('sqrt_6-coth_1-step_6',
            ma.masked_values(
                [[[9999, 9999,    1,    0],
                  [   8,    2,    2,    0],
                  [  20,    5,    5,    0],
                  [9999, 9999, 9999, 9999]],
                 [[9999, 9999,    0,    0],
                  [   7,    2,    1,    1],
                  [  14,    4,    2,    2],
                  [  35,   10, 9999, 9999]],
                 [[9999, 9999, 9999, 9999],
                  [   1,    0, 9999, 9999],
                  [   6,    1, 9999, 9999],
                  [  13,    2, 9999, 9999]]],
                9999),
            outlabel=True,
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮'],
            outputs=[1]),
    Diagram('sqrt_6-coth_1-step_7',
            ma.masked_values(
                [[[9999, 9999, 9999,    1,    0],
                  [9999,    8,    2,    2,    0],
                  [9999,   20,    5,    5,    0],
                  [9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999,    0,    0],
                  [9999,    7,    2,    1,    1],
                  [  74,   14,    4,    2,    2],
                  [ 185,   35,   10, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999],
                  [9999,    1,    0, 9999, 9999],
                  [  31,    6,    1, 9999, 9999],
                  [  67,   13,    2, 9999, 9999]]],
                9999),
            outlabel=True,
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮'],
            outputs=[1]),
    Diagram('sqrt_6-coth_1-step_8',
            ma.masked_values(
                [[[9999, 9999, 9999,    1,    0],
                  [9999,    8,    2,    2,    0],
                  [9999,   20,    5,    5,    0],
                  [9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999,    0,    0],
                  [9999,    7,    2,    1,    1],
                  [  74,   14,    4,    2,    2],
                  [ 185,   35,   10, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999],
                  [9999,    1,    0, 9999, 9999],
                  [  31,    6,    1, 9999, 9999],
                  [  67,   13,    2, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999],
                  [  12,    2, 9999, 9999, 9999],
                  [  51,    9, 9999, 9999, 9999]]],
                9999),
            outlabel=True,
            xs=[1, 3, 5, '…'],
            ys=[2, 2, 4, '⋮'],
            outputs=[1, 2]),
    Diagram('sqrt_6-coth_1-step_8a',
            ma.array(
                [[[ 51,  9],
                  [216, 38]],
                 [[ 16,  4],
                  [ 83, 20]]],
                mask=[False]),
            color_offset=3,
            font_size=SMALL_FONT_SIZE,
            border=0,
            x_padding=0.6,
            y_padding=0.3),
    Diagram('sqrt_6-coth_1-step_9',
            ma.masked_values(
                [[[9999, 9999, 9999, 9999,    1,    0],
                  [9999, 9999,    8,    2,    2,    0],
                  [9999, 9999,   20,    5,    5,    0],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999,    0,    0],
                  [9999, 9999,    7,    2,    1,    1],
                  [9999,   74,   14,    4,    2,    2],
                  [9999,  185,   35,   10, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999,    1,    0, 9999, 9999],
                  [9999,   31,    6,    1, 9999, 9999],
                  [9999,   67,   13,    2, 9999, 9999],
                  [9999,  299,   58, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999,   12,    2, 9999, 9999, 9999],
                  [ 366,   51,    9, 9999, 9999, 9999],
                  [1550,  216,   38, 9999, 9999, 9999],
                  [3466,  483, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [ 116,   16,    4, 9999, 9999, 9999],
                  [ 601,   83,   20, 9999, 9999, 9999],
                  [1318,  182, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [ 348,   50, 9999, 9999, 9999, 9999],
                  [ 830,  119, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [ 253,   33, 9999, 9999, 9999, 9999],
                  [ 488,   63, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [  95,   17, 9999, 9999, 9999, 9999],
                  [ 342,   56, 9999, 9999, 9999, 9999]]],
                9999),
            outlabel=True,
            xs=[1, 3, 5, 7, '…'],
            ys=[2, 2, 4, 2, '⋮'],
            outputs=[1, 2, 1, 2, 1, 1, '⋱']),
    Diagram('real_thing-step_1',
            ma.array(
                [[[ 0, -1],
                  [ 1,  0]],
                 [[-1,  0],
                  [ 0,  1]]],
                mask=[False]),
            xs=[1, 3, 5, 7, '…']),
    Diagram('real_thing-step_2',
            ma.array(
                [[[-1,  0, -1],
                  [ 1,  1,  0]],
                 [[-1, -1,  0],
                  [ 1,  0,  1]]],
                mask=[False]),
            xs=[1, 3, 5, 7, '…']),
    Diagram('real_thing-step_3',
            ma.array(
                [[[-16, -3, -1,  0, -1],
                  [ 21,  4,  1,  1,  0]],
                 [[-21, -4, -1, -1,  0],
                  [ 16,  3,  1,  0,  1]]],
                mask=[False]),
            xs=[1, 3, 5, 7, '…']),
    Diagram('real_thing-step_4',
            ma.masked_values(
                [[[ -16,   -3,   -1,    0,   -1],
                  [  21,    4,    1,    1,    0],
                  [9999, 9999, 9999, 9999, 9999]],
                 [[ -21,   -4,   -1,   -1,    0],
                  [  16,    3,    1,    0,    1],
                  [  11,    2, 9999, 9999, 9999]],
                 [[  26,    5, 9999, 9999, 9999],
                  [ -11,   -2, 9999, 9999, 9999],
                  [   4,    1, 9999, 9999, 9999]]],
                9999),
            xs=[1, 3, 5, 7, '…'],
            ys=[2]),
    Diagram('real_thing-step_5',
            ma.masked_values(
                [[[9999,  -16,   -3,   -1,    0,   -1],
                  [9999,   21,    4,    1,    1,    0],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999,  -21,   -4,   -1,   -1,    0],
                  [ 115,   16,    3,    1,    0,    1],
                  [  79,   11,    2, 9999, 9999, 9999]],
                 [[9999,   26,    5, 9999, 9999, 9999],
                  [ -79,  -11,   -2, 9999, 9999, 9999],
                  [  29,    4,    1, 9999, 9999, 9999]]],
                9999),
            xs=[1, 3, 5, 7, '…'],
            ys=[2]),
    Diagram('real_thing-step_6',
            ma.masked_values(
                [[[9999,  -16,   -3,   -1,    0,   -1],
                  [9999,   21,    4,    1,    1,    0],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999,  -21,   -4,   -1,   -1,    0],
                  [ 115,   16,    3,    1,    0,    1],
                  [  79,   11,    2, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999,   26,    5, 9999, 9999, 9999],
                  [ -79,  -11,   -2, 9999, 9999, 9999],
                  [  29,    4,    1, 9999, 9999, 9999],
                  [  95,   13, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [ 589,   82, 9999, 9999, 9999, 9999],
                  [ -95,  -13, 9999, 9999, 9999, 9999],
                  [  19,    4, 9999, 9999, 9999, 9999]]],
                9999),
            xs=[1, 3, 5, 7, '…'],
            ys=[2, 6]),
    Diagram('real_thing-step_7',
            ma.masked_values(
                [[[9999, 9999,  -16,   -3,   -1,    0,   -1],
                  [9999, 9999,   21,    4,    1,    1,    0],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999,  -21,   -4,   -1,   -1,    0],
                  [9999,  115,   16,    3,    1,    0,    1],
                  [9999,   79,   11,    2, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999,   26,    5, 9999, 9999, 9999],
                  [9999,  -79,  -11,   -2, 9999, 9999, 9999],
                  [ 265,   29,    4,    1, 9999, 9999, 9999],
                  [ 868,   95,   13, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999, 9999],
                  [9999,  589,   82, 9999, 9999, 9999, 9999],
                  [-868,  -95,  -13, 9999, 9999, 9999, 9999],
                  [ 175,   19,    4, 9999, 9999, 9999, 9999],
                  [ 882,   95, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999, 9999],
                  [8945,  979, 9999, 9999, 9999, 9999, 9999],
                  [-882,  -95, 9999, 9999, 9999, 9999, 9999],
                  [ 125,   29, 9999, 9999, 9999, 9999, 9999]]],
                9999),
            xs=[1, 3, 5, 7, 9, '…'],
            ys=[2, 6, 10]),
    Diagram('worked_example-a',
            ma.array(
                [[[0, 6],
                  [1, 0]],
                 [[1, 0],
                  [0, 1]]],
                mask=[False]),
            font_size=SMALL_FONT_SIZE,
            border=0,
            x_padding=0.9,
            y_padding=0.3),
    Diagram('worked_example-step_1',
            ma.masked_values(
                [[[9999,    0,    6],
                  [9999,    1,    0],
                  [9999, 9999, 9999]],
                 [[   2,    1,    0],
                  [   1,    0,    1],
                  [   4,    1, 9999]],
                 [[   2,   -2,    6],
                  [   0,    1,   -2],
                  [   2,    0, 9999]]],
                9999),
            xs=[2, '…'],
            ys=[2, '⋮']),
    Diagram('worked_example-step_2',
            ma.masked_values(
                [[[9999, 9999,    0,    6],
                  [9999, 9999,    1,    0],
                  [9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999]],
                 [[9999,    2,    1,    0],
                  [9999,    1,    0,    1],
                  [9999,    4,    1, 9999],
                  [9999, 9999, 9999, 9999]],
                 [[9999,    2,   -2,    6],
                  [   1,    0,    1,   -2],
                  [   4,    2,    0, 9999],
                  [   9,    4, 9999, 9999]],
                 [[9999, 9999, 9999, 9999],
                  [   0,    1,   -2, 9999],
                  [   1,    0,    1, 9999],
                  [   2,    1, 9999, 9999]]],
                9999),
            xs=[2, 2, '…'],
            ys=[2, 2, '⋮']),
    Diagram('worked_example-step_3',
            ma.masked_values(
                [[[9999, 9999, 9999,    0,    6],
                  [9999, 9999, 9999,    1,    0],
                  [9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999,    2,    1,    0],
                  [9999, 9999,    1,    0,    1],
                  [9999, 9999,    4,    1, 9999],
                  [9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999,    2,   -2,    6],
                  [9999,    1,    0,    1,   -2],
                  [9999,    4,    2,    0, 9999],
                  [9999,    9,    4, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999],
                  [9999,    0,    1,   -2, 9999],
                  [   4,    1,    0,    1, 9999],
                  [   9,    2,    1, 9999, 9999],
                  [  40,    9, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999],
                  [   2,    0,    2, 9999, 9999],
                  [   4,    1,    0, 9999, 9999],
                  [  18,    4, 9999, 9999, 9999]]],
                9999),
            xs=[2, 2, 4, '…'],
            ys=[2, 2, 4, '⋮']),
    Diagram('worked_example-step_4',
            ma.masked_values(
                [[[9999, 9999, 9999, 9999,    0,    6],
                  [9999, 9999, 9999, 9999,    1,    0],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999,    2,    1,    0],
                  [9999, 9999, 9999,    1,    0,    1],
                  [9999, 9999, 9999,    4,    1, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999,    2,   -2,    6],
                  [9999, 9999,    1,    0,    1,   -2],
                  [9999, 9999,    4,    2,    0, 9999],
                  [9999, 9999,    9,    4, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999,    0,    1,   -2, 9999],
                  [9999,    4,    1,    0,    1, 9999],
                  [9999,    9,    2,    1, 9999, 9999],
                  [9999,   40,    9, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999,    2,    0,    2, 9999, 9999],
                  [   9,    4,    1,    0, 9999, 9999],
                  [  40,   18,    4, 9999, 9999, 9999],
                  [  89,   40, 9999, 9999, 9999, 9999]],
                 [[9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [9999, 9999, 9999, 9999, 9999, 9999],
                  [   2,    1,    0, 9999, 9999, 9999],
                  [   9,    4,    1, 9999, 9999, 9999],
                  [  20,    9, 9999, 9999, 9999, 9999]]],
                9999),
            xs=[2, 2, 4, 2, '…'],
            ys=[2, 2, 4, 2, '⋮']),
    Diagram('worked_example-step_5a',
            ma.array(
                [[[40, 18],
                  [89, 40]],
                 [[ 9,  4],
                  [20,  9]],
                 [[ 4,  2],
                  [ 9,  4]],
                 [[ 1,  0],
                  [ 2,  1]]],
                mask=[False]),
            color_offset=4),
    Diagram('worked_example-step_5b',
            ma.array(
                [[[4, 2],
                  [9, 4]],
                 [[1, 0],
                  [2, 1]]],
                mask=[False]),
            color_offset=2),
]


for diagram in DIAGRAMS:
    diagram.position()
    diagram.render()
