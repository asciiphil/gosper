The following files are or are derived from Bill Gosper's paper.  Their
copyright belongs to Bill Gosper:

 * original.txt
 * cf.tex

The remaining files in this repository are licensed under the GNU General
Public License version 3 or, at your option, any later version.  A copy of
the GPL v3 is in the `GPL-3.0.md` file.