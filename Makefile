
TEX_SOURCE := cf.tex
DIAGRAMS := $(shell perl -nle '/includegraphics/ and /{([^{}]+.pdf)}/ and print $$1' $(TEX_SOURCE))

cf.pdf: cf.tex $(DIAGRAMS)
	rubber -m xelatex $<

$(DIAGRAMS): draw-3d-matrices.py
	./draw-3d-matrices.py

clean:
	rm -f *.pdf cf.aux cf.log cf.out
